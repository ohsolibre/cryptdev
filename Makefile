CC = cc
CFLAGS = -Wall -Wextra -Os
LDFLAGS = -static -s -lcrypto

cryptdev: cryptdev.c
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)
clean:
	rm -f cryptdev
